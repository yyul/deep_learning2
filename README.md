# Deep Learning 2 : RNN, LSTM, GAN

* Implement the forward and backward passes as well as the neural network training procedure for Recurrent Neural Networks (RNNs)
* Basic concepts of language modeling and apply RNNs
* Implement popular generative model, Generative Adversarial Networks (GANs)

```
# Install dependencies
pip3 install -r requirements.txt
```

### RNN, LSTM :
The IPython Notebook `rnn_lstm.ipynb` : implementing a recurrent neural network (RNN) from scratch.

### Generative Adversarial Networks :
The IPython Notebook `gan.ipynb` : implementing a generative adversarial network (GAN) using TensorFlow.
